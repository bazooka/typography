# Typography

## Présentation
L'élément HTML ```<p>``` représente un paragraphe de texte. Les paragraphes sont généralement représentés comme des blocs et séparés par un espace vertical, leur première ligne est également parfois indentée.
Il y a également l'élément ```<code>``` représente un fragment de code machine. Par défaut, l'agent utilisateur utilise une police à chasse fixe (monospace) afin d'afficher le texte contenu dans cet élément.
Typography regrouppe ces différents éléments de texte.

## Utilisation

### Paramètres utilisables : 

#### Les variables
Voici les variables par défaut

| Nom              | Valeur                                                                |
|-----------------------------|----------------------------------------------------------------------------|
| ```$code-bg-color```     | ```#FFF !default``` |
| ```$code-text-color``` | ```#000 !default``` |
| ```$headings``` | ``` ( 'h1': (), 'h2': (), 'h3': (), 'h4': (), 'h5': (), 'h6': () ) !default;``` |

Vous pouvez les modifier en fonction de votre charte


#### Les options

##### Paragraphes
Pour styliser les paragraphes par défaut, vous pouvez utiliser ```@include p()```

Pour styliser des textes avec une taille de police moins importante comme par exemple avec le tag `<small>`, vous pouvez utiliser ```@include small()```

Pour styliser des textes avec une taille de police plus impactante comme par exemple un chapô d'article, vous pouvez utiliser ```@include large()```

##### Code
Pour le type code, vous pouvez utiliser ```@include code($bg-color: #FFF, $text-color: #000)```

##### Titres
Pour chaque niveau de titre, vous pouvez utiliser la mixin qui lui est associée, ```@include h1()``` pour le `<h1>` par exemple.

Il est possible de surcharger les valeurs de font-size et line-height de chaque titre en fonction des breakpoints.

L'exemple suivant agira sur la font-size (1ère valeur dans la liste) et la line-height (2ème valeur dans la liste) :
```
'h1': (
        'small': (34px, 40px),
        'large': (56px, 64px)
)
```

L'exemple suivant agira uniquement sur la font-size :
```
'h2': (
    'small': 22px,
    'large': 46px
)
```

## Installation
Pour importer le composant dans votre projet, il vous faut au préalable ```node```.

```npm i git+ssh://git@gitlab.com:bazooka/typography```

Puis importer les différents fichiers :
* code ```@import "~@bazooka/typography/code"```
* heading ```@import "~@bazooka/typography/heading"```
* paragraph ```@import "~@bazooka/typography/paragraph"```

## Développement
Le développement se fait sur la branche ```develop```.
